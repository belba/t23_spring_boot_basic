package t23_spring_boot_basic.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T23SpringBootBasicApplication {

	public static void main(String[] args) {
		SpringApplication.run(T23SpringBootBasicApplication.class, args);
	}

}
