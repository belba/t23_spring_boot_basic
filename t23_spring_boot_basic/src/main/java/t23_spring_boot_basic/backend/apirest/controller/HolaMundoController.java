package t23_spring_boot_basic.backend.apirest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HolaMundoController {
	@RequestMapping("/")
	public String saludos() {
		return "index";
	}
	
	@RequestMapping("/saludos")
	public String hola() {
		return "index";
	}
}
